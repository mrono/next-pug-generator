const requireField = fieldName => {
  return value => {
    if (String(value).length === 0) {
      return fieldName + ' is required'
    }
    return true
  }
}

var base_path = 'client/';

function pcase(s) {
  if (!s) {
    return;
  }
  return s.replace(/(\w)(\w*)/g, function (g0, g1, g2) { return g1.toUpperCase() + g2.toLowerCase(); });
}

module.exports = plop => {
  plop.setHelper('extract_name', function (var_name) {
    return pcase(var_name.split('/').pop());
  });
  plop.setHelper('format_path', function (var_name) {
    var folders = var_name.split('/');
    var name = folders.pop();

    return `${folders ? folders.join('/') + '/' : ''}${name}`;
  });
  plop.setHelper('ifNEquals', function (arg1, arg2, options) {
    return (arg1 != arg2) ? options.fn(this) : options.inverse(this);
  });
  plop.setGenerator('component', {
    description: 'Create a reusable component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
        validate: requireField('name')
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'client/components/{{format_path name}}.js',
        templateFile:
          'plop-templates/component.js.hbs',
      }
    ],
  })

  plop.setGenerator('page', {
    description: 'Create a page',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your page name?',
        validate: requireField('name')
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'client/pages/{{format_path name}}.js',
        templateFile:
          'plop-templates/page.js.hbs',
      },
      // {
      //   type: 'append',
      //   path: 'src/pages/index.js',
      //   pattern: `/* PLOP_INJECT_IMPORT */`,
      //   template: `import {{pascalCase name}} from './{{pascalCase name}}';`,
      // }
    ]
  })


  plop.setGenerator('api', {
    description: 'Create an api route',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your route name?',
        validate: requireField('name')
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'client/pages/api/{{format_path name}}.js',
        templateFile:
          'plop-templates/api_page.js.hbs',
      },
    ]
  })


  plop.setGenerator('context', {
    description: 'Create an api route',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your context name? (exclude "Context")',
        validate: requireField('name')
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'client/components/context/{{pascalCase name}}Context.js',
        templateFile:
          'plop-templates/context.js.hbs',
      }
    ]
  })


  plop.setGenerator('service', {
    description: 'Create a service for api routes',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your service name?',
        validate: requireField('name')
      },
      {
        type: 'confirm',
        name: 'model',
        message: 'Do you want to link to a model? Y/n:',
        validate: requireField('model')
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'client/services/{{pascalCase name}}.js',
        templateFile:
          'plop-templates/service.js.hbs',
      }
    ]
  })


  plop.setGenerator('init', {
    description: 'init ',
    prompts: [
      {
        type: 'confirm',
        name: 'packages',
        message: 'Please run: npm install next react react-dom swr nedb-modules'
      },
      {
        type: 'confirm',
        name: 'dev-packages',
        message: 'Please run: npm install --save-dev babel-plugin-transform-jsx-classname-components babel-plugin-transform-react-pug babel-eslint eslint eslint-config-airbnb eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-react eslint-plugin-react-hooks eslint-plugin-react-pug jest plop webpack'
      }
    ],
    actions: [
      {
        type: 'add',
        path: 'client/.babelrc',
        templateFile:
          'plop-templates/init/.babelrc.hbs',
      },
      {
        type: 'add',
        path: 'client/pages/_app.js',
        templateFile:
          'plop-templates/init/_app.js.hbs',
      },
      {
        type: 'add',
        path: 'client/jsconfig.json',
        templateFile:
          'plop-templates/init/jsconfig.json.hbs',
      },
      {
        type: 'add',
        path: 'client/components/layout.js',
        templateFile:
          'plop-templates/init/layout.js.hbs',
      },
      {
        type: 'add',
        path: 'client/.eslintrc.json',
        templateFile:
          'plop-templates/init/.eslintrc.json',
      },
      {
        type: 'add',
        path: 'client/services/fetcher.js',
        templateFile:
          'plop-templates/init/fetcher.js.hbs',
      },
      {
        type: 'add',
        path: 'index.js',
        templateFile:
          'plop-templates/init/index.js.hbs',
      },
      {
        type: 'append',
        path: '.gitignore',
        template: `.next/`,
      }
    ]
  })
}
